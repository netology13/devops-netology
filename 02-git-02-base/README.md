# Домашнее задание к занятию «2.2. Основы Git»

## Задание №1 – Знакомимся с gitlab и bitbucket 

   ![git_remote](.README_images/git_remote.png)

## Задание №2 – Теги
1. https://github.com/

   ![tags_github](.README_images/tags_github.png)

2. https://gitlab.com/

   ![tags_gitlab](.README_images/tags_gitlab.png)

3. https://bitbucket.org/

   ![tags_bitbucket](.README_images/tags_bitbucket.png)

## Задание №3 – Ветки 

   ![git_network](.README_images/git_network.png)



