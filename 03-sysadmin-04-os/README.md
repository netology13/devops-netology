# Домашнее задание к занятию "3.4. Операционные системы, лекция 2"

1. На лекции мы познакомились с [node_exporter](https://github.com/prometheus/node_exporter/releases). В демонстрации его исполняемый файл запускался в background. Этого достаточно для демо, но не для настоящей production-системы, где процессы должны находиться под внешним управлением. Используя знания из лекции по systemd, создайте самостоятельно простой [unit-файл](https://www.freedesktop.org/software/systemd/man/systemd.service.html) для node_exporter:

    * поместите его в автозагрузку,
    * предусмотрите возможность добавления опций к запускаемому процессу через внешний файл (посмотрите, например, на `systemctl cat cron`),
    * удостоверьтесь, что с помощью systemctl процесс корректно стартует, завершается, а после перезагрузки автоматически поднимается.

*Node Exporter установлен и запускается*

![node_exporter_installed](.README_images/node_exporter.png)

*создан unit-файл для Node Exporter предусматривающий добавление опций `$OPTIONS` к запускаемому процессу через внешний файл `/etc/sysconfig/node_exporter`*

![unit-file](.README_images/node_exporter_service.png)

*командой `sudo systemctl enable node_exporter` Node Exporter добавлен в автозагрузку*

*node exporter корректно стартует, завершается и после перезагрузки автоматически поднимается*

![checking](.README_images/node_exporter_stable.png)

*у Node Exporter имеется переменная окружения для добавления опций к запускаемому процессу*

![environment](.README_images/node_exporter_environment.png)

*node exporter после перезагрузки виртуальной машины автоматически поднимается*

![after_reload](.README_images/node_exporter_reload.png)

2. Ознакомьтесь с опциями node_exporter и выводом `/metrics` по-умолчанию. Приведите несколько опций, которые вы бы выбрали для базового мониторинга хоста по CPU, памяти, диску и сети.

```bash
--collector.disable-defaults
--collector.cpu
--collector.cpufreq
--collector.meminfo
--collector.diskstats
--collector.netstat
```
3. Установите в свою виртуальную машину [Netdata](https://github.com/netdata/netdata). Воспользуйтесь [готовыми пакетами](https://packagecloud.io/netdata/netdata/install) для установки (`sudo apt install -y netdata`). После успешной установки:
    * в конфигурационном файле `/etc/netdata/netdata.conf` в секции [web] замените значение с localhost на `bind to = 0.0.0.0`,
    * добавьте в Vagrantfile проброс порта Netdata на свой локальный компьютер и сделайте `vagrant reload`:

    ```bash
    config.vm.network "forwarded_port", guest: 19999, host: 19999
    ```

    После успешной перезагрузки в браузере *на своем ПК* (не в виртуальной машине) вы должны суметь зайти на `localhost:19999`. Ознакомьтесь с метриками, которые по умолчанию собираются Netdata и с комментариями, которые даны к этим метрикам.

*Netdata установлена на виртуальной машине и открывается с хоста*

![netdata](.README_images/netdata.png)

4. Можно ли по выводу `dmesg` понять, осознает ли ОС, что загружена не на настоящем оборудовании, а на системе виртуализации?

*Да, данная информация содержится в `dmesg`*

![dmesg](.README_images/dmesg_hv.png)

![dmesg2](.README_images/dmesg_detected.png)

5. Как настроен sysctl `fs.nr_open` на системе по-умолчанию? Узнайте, что означает этот параметр. Какой другой существующий лимит не позволит достичь такого числа (`ulimit --help`)?

![fs_nr_open](.README_images/fs_nr_open.png)

*`fs.nr_open` устанавливает системное ограничение на максимальное число открываемых файлов (аллоцируемых файловых дескрипторов).*

*команды `ulimit -Sn` и `ulimit -Hn` отображают soft (данный параметр можно увеличить системным вызовом `setrlimit` до пределов установленных в переменной hard) и hard значение вышеназванного ограничения устанавливаемого на сессионном уровне.*

*Дополнительно, параметром `/proc/sys/fs/file-max` устанавливается максимальное ограничение на количество файловых дескрипторов аллоцируемых ядром.*

6. Запустите любой долгоживущий процесс (не `ls`, который отработает мгновенно, а, например, `sleep 1h`) в отдельном неймспейсе процессов; покажите, что ваш процесс работает под PID 1 через `nsenter`. Для простоты работайте в данном задании под root (`sudo -i`). Под обычным пользователем требуются дополнительные опции (`--map-root-user`) и т.д.

![nsenter](.README_images/nsenter.png)

7. Найдите информацию о том, что такое `:(){ :|:& };:`. Запустите эту команду в своей виртуальной машине Vagrant с Ubuntu 20.04 (**это важно, поведение в других ОС не проверялось**). Некоторое время все будет "плохо", после чего (минуты) – ОС должна стабилизироваться. Вызов `dmesg` расскажет, какой механизм помог автоматической стабилизации. Как настроен этот механизм по-умолчанию, и как изменить число процессов, которое можно создать в сессии?

![forkbomb](.README_images/fork_rejected.png)

*Работу форкбомбы прервал Process Number Controller*

![default_task_pid_max](.README_images/default_task_max.png)

*Максимальное количество процессов для пользователя можно изменить командой `ulimit -u <число>` или в файле cat `etc/security/limits.conf`*

*Изменить максимальное количество PID можно посредством команд `sysctl -w kernel.pid_max=<число>`,`echo <число> > /proc/sys/kernel/pid_max` или задать переменную `kernel.pid_max` в файле  `/etc/sysctl.conf`*

*Ограничение на максимальное число процессов на уровне системы установлено в переменной DefaultTasksMax: `systemctl show --property DefaultTasksMax` изменить данную переменную можно в файле `/etc/systemd/system.conf`*

*Переменная `UserTasksMax` в файле `/etc/systemd/logind.conf` позволяет установить ограничение по максимальному количеству процессов на уровне пользователей* 