# Домашнее задание к занятию «2.4. Инструменты Git»

Для выполнения заданий в этом разделе давайте склонируем репозиторий с исходным кодом 
терраформа https://github.com/hashicorp/terraform 

В виде результата напишите текстом ответы на вопросы и каким образом эти ответы были получены. 

1. Найдите полный хеш и комментарий коммита, хеш которого начинается на `aefea`.

```aefead2207ef7e2aa5dc81a34aedf0cad4c32545``` <br> ```Update CHANGELOG.md```

2. Какому тегу соответствует коммит `85024d3`?

```tag: v0.12.23```

3. Сколько родителей у коммита `b8d720`? Напишите их хеши.

```parent 56cd7859e05c36c06b56d013b55a252d0bb7e158``` <br>
```parent 9ea88f22fc6269854151c571162c5bcf958bee2b```


4. Перечислите хеши и комментарии всех коммитов которые были сделаны между тегами  v0.12.23 и v0.12.24.

    2. ```b14b74c4939dcab573326f4e3ee2a62e23e12f89``` <br> ```[Website] vmc provider links``` 

    3. ```3f235065b9347a758efadc92295b540ee0a5e26e``` <br> ```Update CHANGELOG.md```

    4. ```6ae64e247b332925b872447e9ce869657281c2bf``` <br> ```registry: Fix panic when server is unreachable```

    5. ```5c619ca1baf2e21a155fcdb4c264cc9e24a2a353``` <br> ```website: Remove links to the getting started guide's old location```

    6. ```06275647e2b53d97d4f0a19a0fec11f6d69820b5``` <br> ```Update CHANGELOG.md```

    7. ```d5f9411f5108260320064349b757f55c09bc4b80``` <br> ```command: Fix bug when using terraform login on Windows```

    8. ```4b6d06cc5dcb78af637bbb19c198faff37a066ed``` <br> ```Update CHANGELOG.md```

    9. ```dd01a35078f040ca984cdd349f18d0b67e486c35``` <br> ```Update CHANGELOG.md```

    10. ```225466bc3e5f35baa5d07197bbc079345b77525e``` <br> ```Cleanup after v0.12.23 release```


5. Найдите коммит в котором была создана функция `func providerSource`, ее определение в коде выглядит 
так `func providerSource(...)` (вместо троеточего перечислены аргументы).

   ```commit 8c928e83589d90a031f811fae52a81be7153e82f```


7. Найдите все коммиты в которых была изменена функция `globalPluginDirs`.

   1. ```commit 78b12205587fe839f10d946ea3fdc06719decb05```
   2. ```commit 52dbf94834cb970b510f2fba853a5b49ad9b1a46```
   3. ```commit 41ab0aef7a0fe030e84018973a64135b11abcd70``` 
   4. ```commit 66ebff90cdfaa6938f26f908c7ebad8d547fea17```
   5. ```commit 8364383c359a6b738a436d1b7745ccdce178df47```
     

8. Кто автор функции `synchronizedWriters`?

```
commit 5ac311e2a91e381e2f52234668b49ba670aa0fe5
Author: Martin Atkins <mart@degeneration.co.uk>
Date:   Wed May 3 16:25:41 2017 -0700
```