#!/usr/bin/env bash
exec 3>&1 4>&2
trap 'exec 2>&4 1>&3' 0 1 2 3
exec 1>>/home/vagrant/cert_update.logfile 2>&1

set -euxo pipefail

date -Iseconds

cert_data=/home/vagrant/cert.json

export VAULT_ADDR=http://0.0.0.0:8200
export VAULT_TOKEN=root
# shellcheck disable=SC2046

vault write -format=json pki_int/issue/example-dot-com common_name="test.example.com>

cat $cert_data | jq -r .data.certificate > /etc/nginx/ssl/test.example.com.crt
cat $cert_data | jq -r .data.issuing_ca >> /etc/nginx/ssl/test.example.com.crt
cat $cert_data | jq -r .data.private_key > /etc/nginx/ssl/test.example.com.key

systemctl restart nginx

cat $cert_data | jq -r .data.serial_number

rm $cert_data
