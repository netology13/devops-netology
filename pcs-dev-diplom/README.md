# Курсовая работа по итогам модуля "DevOps и системное администрирование"

Курсовая работа необходима для проверки практических навыков, полученных в ходе прохождения курса "DevOps и системное администрирование".

Мы создадим и настроим виртуальное рабочее место. Позже вы сможете использовать эту систему для выполнения домашних заданий по курсу

## Задание

1. Создайте виртуальную машину Linux.

![linux_created](.README_images/linux_created.png)

2. Установите ufw и разрешите к этой машине сессии на порты 22 и 443, при этом трафик на интерфейсе localhost (lo) должен ходить свободно на все порты.

![ufw_installed](.README_images/ufw_installed.png)

![ufw_setup](.README_images/ufw_setup.png)

3. Установите hashicorp vault ([инструкция по ссылке](https://learn.hashicorp.com/tutorials/vault/getting-started-install?in=vault/getting-started#install-vault)).

![vault](.README_images/vault.png)

4. Cоздайте центр сертификации по инструкции ([ссылка](https://learn.hashicorp.com/tutorials/vault/pki-engine?in=vault/secrets-management)) и выпустите сертификат для использования его в настройке веб-сервера nginx (срок жизни сертификата - месяц).

![vault_setup](.README_images/vault_setup.png)

![vault_root_CA](.README_images/vault_root_CA.png)

![vault_intermidate_CA](.README_images/vault_intermidate_CA.png)

![vault_role_and_request](.README_images/vault_role_and_request.png)

5. Установите корневой сертификат созданного центра сертификации в доверенные в хостовой системе.

![installed_cert](.README_images/installed_cert.png)

6. Установите nginx.

![nginx](.README_images/nginx.png)

7. По инструкции ([ссылка](https://nginx.org/en/docs/http/configuring_https_servers.html)) настройте nginx на https, используя ранее подготовленный сертификат:

- можно использовать стандартную стартовую страницу nginx для демонстрации работы сервера;
- можно использовать и другой html файл, сделанный вами;

![nginx_certs](.README_images/nginx_certs.png)

nginx config:

```bash
server {
	listen 443 ssl;
	server_name test.example.com;
	ssl_certificate	/etc/nginx/ssl/test.example.com.crt;
	ssl_certificate_key /etc/nginx/ssl/test.example.com.key;
	#...
}
```

8. Откройте в браузере на хосте https адрес страницы, которую обслуживает сервер nginx.

![cert_valid](.README_images/cert_valid.png)

![connection_secure](.README_images/connection_secure.png)

9. Создайте скрипт, который будет генерировать новый сертификат в vault:

- генерируем новый сертификат так, чтобы не переписывать конфиг nginx;
- перезапускаем nginx для применения нового сертификата.

![update_sh](.README_images/update_sh.png)

10. Поместите скрипт в crontab, чтобы сертификат обновлялся какого-то числа каждого месяца в удобное для вас время.

Запуск первого числа каждого месяца:

![every_month](.README_images/monthly.png)

Запуск раз в минуту для тестирования:

![every_minute](.README_images/every_minute.png)

Логфайл:

![logfile](.README_images/logfile.png)
