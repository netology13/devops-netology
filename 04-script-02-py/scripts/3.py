#!/usr/bin/env python3

import os
import sys

if len(sys.argv) > 1:
    bash_command = ["cd " + sys.argv[1], "git rev-parse --is-inside-work-tree", "git ls-files -m --full-name"]
else:
    bash_command = ["cd $(git rev-parse --show-toplevel)", "git rev-parse --is-inside-work-tree", "git ls-files -m --full-name"]

result_os = os.popen(' && '.join(bash_command)).read()
for result in result_os.split('\n'):
    if result == 'true':
        print('it\'s a git repo')
        continue
    if result is not '':
        print(os.path.abspath(result))