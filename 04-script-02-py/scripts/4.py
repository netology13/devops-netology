#!/usr/bin/env python3

import socket
import time

dns_list = ['drive.google.com', 'mail.google.com', 'google.com']
ip_list = [None, None, None]

while True:

    for i in range(0, len(dns_list)):
        time.sleep(1)
        ip = socket.gethostbyname(dns_list[i])
        print(dns_list[i] + ' -> ' + ip)
        if ip_list[i] is None:
            ip_list[i] = ip
        elif ip_list[i] != ip:
            print(dns_list[i] + ' IP changed from: ' + ip_list[i] + ' to: ' + ip)

