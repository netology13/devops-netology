# Заменить на ID своего облака
# https://console.cloud.yandex.ru/cloud?section=overview
variable "yandex_cloud_id" {
  default = "b1ggqoer0ffn86fddr21"
}

# Заменить на Folder своего облака
# https://console.cloud.yandex.ru/cloud?section=overview
variable "yandex_folder_id" {
  default = "b1gf4s4n3sm4lle1et77"
}

# Заменить на ID сервисного аккаунта
# ID можно узнать с помощью команды yc iam service-account list
variable "service_account_id" {
  default = "ajekij3j0c0dqcc0q2ah"
}