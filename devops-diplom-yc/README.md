# Дипломный практикум в YandexCloud

## Цели:

1. Зарегистрировать доменное имя (любое на ваш выбор в любой доменной зоне).
2. Подготовить инфраструктуру с помощью Terraform на базе облачного провайдера YandexCloud.
3. Настроить внешний Reverse Proxy на основе Nginx и LetsEncrypt.
4. Настроить кластер MySQL.
5. Установить WordPress.
6. Развернуть Gitlab CE и Gitlab Runner.
7. Настроить CI/CD для автоматического развёртывания приложения.
8. Настроить мониторинг инфраструктуры с помощью стека: Prometheus, Alert Manager и Grafana.

## Этапы выполнения:

### 1. Регистрация доменного имени

Получен доступ к личному кабинету на сайте регистратора.
![domain_name](.README_images/domain_name.png)

Домен зарезервирован _(netologydiplom.ru)_, имеется возможность им управлять (редактировать dns записи в рамках этого домена).
![domain_settings](.README_images/domain_settings.png)

### 2. Создание инфраструктуры

#### Предварительная подготовка:

1. Создание сервисного аккаунта с ролью Editor для работы с инфраструктурой посредством Terraform.

![yc_init](.README_images/yc_init.png)

Создадим сервисный аккаунт согласно документации: [https://cloud.yandex.ru/docs/iam/operations/sa/create](https://cloud.yandex.ru/docs/iam/operations/sa/create)

![service_account](.README_images/service_account.png)

Зададим роль Editor для сервисного аккаунта согласно документации: [https://cloud.yandex.ru/docs/iam/operations/sa/assign-role-for-sa](https://cloud.yandex.ru/docs/iam/operations/sa/assign-role-for-sa)

![service_account_role](.README_images/service_account_role.png)

2. Подготовим backend для Terraform. Для хранения состояний будет использоваться Yandex Object Storage.

Ссылка на соответствующий раздел документации: [Загрузка состояний Terraform в Object Storage](https://cloud.yandex.ru/docs/tutorials/infrastructure-management/terraform-state-storage)

Создадим bucket посредством Terraform согласно документации: [Создание бакета](https://cloud.yandex.ru/docs/storage/operations/buckets/create)

Отредактируем файл конфигурации Terraform добавив в него зеркало yandexcloud:

```
provider_installation {
  network_mirror {
    url = "https://terraform-mirror.yandexcloud.net/"
    include = ["registry.terraform.io/*/*"]
  }
  direct {
    exclude = ["registry.terraform.io/*/*"]
  }
}
```

Создадим IAM ключ сервисного аккаунта согласно документации [yc iam key create](https://cloud.yandex.ru/docs/cli/cli-ref/managed-services/iam/key/create):

![key_file_create](.README_images/key_file_create.png)

Отредактируем [конфигурационный файл](src/stage/terraform/provider.tf) Terraform приведённый в [документации](https://cloud.yandex.ru/docs/storage/operations/buckets/create) для создания object storage.

Результат исполнения команды Terraform apply:

![terraform_apply](.README_images/terraform_apply.png)

3. Создадим рабочее пространство Terraform:

![tf_workspaces](.README_images/tf_workspaces.png)

4. Создадим VPC с двумя подсетями в зонах "ru-central1-a" и "ru-central1-b" руководствуясь документацией ([Создать облачную сеть](https://cloud.yandex.ru/docs/vpc/operations/network-create), [Создать подсеть](https://cloud.yandex.ru/docs/vpc/operations/subnet-create)):

<details>
<summary>Изначальный листинг файла networks.tf</summary>
```
# Network
resource "yandex_vpc_network" "default" {
  name = "net"
}
resource "yandex_vpc_subnet" "subnet" {
count                   = 2
name                    = format("subnet-%02d", count.index + 1)
zone                    = var.zones[count.index]
network_id              = yandex_vpc_network.default.id
v4_cidr_blocks          = [var.CIDR_blocks[count.index]]
}
```
</details>

5. После копирования значений _access_key_ и _secret_key_ в файл backend.conf(включен в .gitignore), полученных после генерации хранилища, выполним команды ```terraform init -backend-config=backend.conf```, ```terraform validate```, ```terraform plan -var-file="tfvariables.tfvars"```,```terraform apply -var-file="tfvariables.tfvars"```:

![terraform_apply_first](.README_images/terraform_apply_first.png)

Результат в консоли yandex cloud:

![yc_result](.README_images/yc_result.png)

### 2. Установка Nginx и LetsEncrypt

Зарезервируем статический публичный IP-адрес согласно документации: [Зарезервировать статический публичный IP-адрес](https://cloud.yandex.ru/docs/vpc/operations/get-static-ip):

![static_ip](.README_images/static_ip.png)

2.1. Создадим конфигурационные файлы Terraform виртуальных машин указанных в задании:

2.1.1. Proxy:

• Имя сервера: you.domain

• Характеристики: 2vCPU, 2 RAM, External address (Public) и Internal address.

2.1.2. Кластер MySQL:

• Имена серверов: db01.you.domain и db02.you.domain

• Характеристики: 4vCPU, 4 RAM, Internal address.

2.1.3. Веб сервер WordPress:

• Имя сервера: app.you.domain

• Характеристики: 4vCPU, 4 RAM, Internal address.

2.1.4. Сервер Gitlab CE и Gitlab Runner:

• Имена серверов: gitlab.you.domain и runner.you.domain

• Характеристики: 4vCPU, 4 RAM, Internal address.

2.1.5. Сервер мониторинга Prometheus, Alert Manager, Node Exporter и Grafana:

• Имя сервера: monitoring.you.domain

• Характеристики: 4vCPU, 4 RAM, Internal address.

2. На сайте регистратора установим публичные зоны DNS Yandex cloud указанные в [документации](https://cloud.yandex.ru/docs/dns/concepts/dns-zone):

![dns_reg_ru](.README_images/dns_reg_ru.png)

3. Посредством конфигурационного файла Terraform _dns.tf_ зададим ресурсные записи в Yandex Cloud DNS согласно документации: [https://cloud.yandex.ru/docs/dns/operations/zone-create-public](https://cloud.yandex.ru/docs/dns/operations/zone-create-public), дополнительно добавим NAT правила в файл описывающий сетевую инфраструктуру.

Созданные виртуальные машины:

![vms](.README_images/vms.png)

Созданные DNS записи:

![dns_records](.README_images/dns_records.png)

4. Используем Ansible роль [https://github.com/geerlingguy/ansible-role-certbot/](https://github.com/geerlingguy/ansible-role-certbot/) для установки Nginx и LetsEncrypt, дополнительно сформируем inventory файл для передачи сетевых адресов в Ansible.

Для корректной работы ansible помимо ansible-core необходимо установить модуль posix [https://galaxy.ansible.com/ansible/posix](https://galaxy.ansible.com/ansible/posix)

Ответ сервера 502:

![502_proxy](.README_images/502_proxy.png)

Скриншот сертификата Let's encrypt:

![encrypt](.README_images/encrypt.png)

После обновления DNS кэша сайт открывается по символьной ссылке, сертификат сайта добавлен в доверенные:

![dns_resolved](.README_images/dns_resolved.png)

###4. Установка кластера MySQL

4.1 Для создания кластера использована модифицированная роль Ansible - [https://github.com/geerlingguy/ansible-role-mysql/](https://github.com/geerlingguy/ansible-role-mysql/)

![mysql_stats](.README_images/mysql_stats.png)

4.2 В кластере автоматически создаётся база данных wordpress:

![wordpress_db](.README_images/wordpress_db.png)

4.3 В кластере автоматически создаётся пользователь wordpress с полными правами на базу wordpress и паролем wordpress:

![mysql_user](.README_images/mysql_user.png)

###5. Установка Wordpress:

5.1 Посредством ansible роли на виртуальной машине установлен nginx:

![nginx_version](.README_images/nginx_version.png)

5.2 Посредством ansible роли на виртуальной машине установлен wordpress:

![wp_version](.README_images/wp_version.png)

5.3 В доменной зоне настроена A-запись на внешний адрес reverse proxy:

![wp_dns](.README_images/wp_dns.png)

5.4 В браузере можно открыть URL https://www.you.domain и увидеть главную страницу WordPress:

![wp_install](.README_images/wp_install.png)

![word_press](.README_images/word_press.png)

###6. Установка Gitlab CE и Gitlab Runner

6.1 После выполнения ansible роли интерфейс Gitlab доступен по https:

![gitlab_https](.README_images/gitlab_https.png)

![gitlab_runners](.README_images/gitlab_runners.png)

![wp_project_gitlab](.README_images/wp_project_gitlab.png)

![netologydiplom-project](.README_images/netologydiplom-project.png)

Запушим содержимое папки wordpress в созданный репозиторий:

![git_push](.README_images/git_push.png)

Добавим закрытую часть ssh ключа (созданного без passphrase):

![ssh_added](.README_images/ssh_added.png)

Посредством ansible добавим runner'a подкинув регистрационный токен:

![runner_added](.README_images/runner_added.png)

При добавлении файла в репозиторий путём коммита с тэгом, pipline посредством rsync загрузит файл на web сервер.

![pulled_text](.README_images/pulled_text.png)

Скриншоты успешного выполнения задач:

Задача выполняемая при коммите без тэга:

![without_tag](.README_images/without_tag.png)

Задача выполняемая при коммите с тэгом:

![job_executed_when_tag](.README_images/job_executed_when_tag.png)

Общий вид выполняемых задач:

![jobs_with_tag](.README_images/jobs_with_tag.png)

### 7.Gitlab, grafana:

![prometheus_stats](.README_images/prometheus_stats.png)

![grafana](.README_images/grafana.png)

![alertmanager](.README_images/alertmanager.png)

![prometheus](.README_images/prometheus.png)
